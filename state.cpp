#include "state.h"

State::State()
  : wheelLength(0.0), startMillis(0), count(0), previousRotationTime(0),
    lastRotationTime(0), currentSpeed(0.0), maximumSpeed(0.0) {}

double State::getDistance() const {
  double distance = count * wheelLength / 1000.0;
  return (int)(distance * 100) / 100.0;
}

double State::getCurrentSpeed() const {
  return currentSpeed;
}

unsigned int State::getSecondsPassed() const {
  return (millis() - startMillis) / 1000;
}

String State::getTime() const {
  int secondsPassed = getSecondsPassed();
  int seconds = secondsPassed % 60;
  int minutes = (secondsPassed / 60) % 60;
  int hours = (secondsPassed / 3600);
  return toTimeString(hours) + ":" + toTimeString(minutes) + ":" + toTimeString(seconds);
}

void State::recalculateSpeed() {
  if (previousRotationTime > 0) {
    double secondsElapsed = (lastRotationTime - previousRotationTime) / 1000.0;
    currentSpeed = wheelLength / secondsElapsed * 3.6;
  }
  if (currentSpeed > maximumSpeed) {
    maximumSpeed = currentSpeed;
  }
}

String State::toTimeString(int value) const {
  String time = String(value);
  if (time.length() < 2) {
    time = "0" + time;
  }
  return time;
}
void State::init(double wheelLength, unsigned long startMillis) {
  this->wheelLength = wheelLength;
  this->startMillis = startMillis;
}

void State::addRotation(unsigned long millis) {
  previousRotationTime = lastRotationTime;
  lastRotationTime = millis;
  count++;
  recalculateSpeed();
}

void State::display() const {
  Serial.print("Count: ");
  Serial.print(count);

  Serial.print("\tDistance: ");
  Serial.print((int)(getDistance() * 100) / 100.0);
  Serial.print(" km");

  Serial.print("\tTime: ");
  Serial.print(getTime());

  Serial.print("\tSpeed: ");
  Serial.print(getCurrentSpeed());
  Serial.print(" km/h");

  Serial.print("\Maximum: ");
  Serial.print(maximumSpeed);
  Serial.println(" km/h");
}
