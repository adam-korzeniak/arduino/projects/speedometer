#ifndef STATE_H
#define STATE_H

#include <Arduino.h>

class State {
private:
  double wheelLength;
  unsigned long startMillis;
  unsigned int count;
  unsigned long previousRotationTime;
  unsigned long lastRotationTime;
  double currentSpeed;
  double maximumSpeed;

  double getDistance() const;
  double getCurrentSpeed() const;
  unsigned int getSecondsPassed() const;
  String getTime() const;
  String toTimeString(int value) const;
  void recalculateSpeed();

public:
  State();

  void init(double wheelLength, unsigned long startMillis);
  void addRotation(unsigned long millis);
  void display() const;
};
#endif
