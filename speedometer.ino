#include "state.h"

constexpr int SENSOR_PIN = 7;
constexpr double WHEEL_LENGTH = 2.1;

int previousSensorState = LOW;
State state;

void setup() {
  Serial.begin(9600);

  pinMode(SENSOR_PIN, INPUT);

  state.init(WHEEL_LENGTH, millis());
}

void loop() {
  int sensorState = digitalRead(SENSOR_PIN);
  displaySensorState(sensorState);
  //TODO: Add debouncing
  if (sensorState == HIGH && previousSensorState == LOW) {
    state.addRotation(millis());
  }
  previousSensorState = sensorState;

  state.display();
}

void displaySensorState(int sensorState) {
  Serial.println("State: " + sensorState);
}
